package workshop;

	public class Desk extends Workshop {
		public Desk(Toys subject){
		      this.subject = subject;
		      this.subject.attach(this);
		}

		@Override
		public void update() {
			if(subject.getState() == "Трябват ми кукли") {
				System.out.println("Дъска : Кукла");	
				}
				if(subject.getState() == "Трябват ми колелета") {
					System.out.println("Дъска : Колело");	
					}
		}    	
	}


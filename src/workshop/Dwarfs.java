package workshop;

public class Dwarfs extends Workshop {
	public Dwarfs(Toys subject){
	      this.subject = subject;
	      this.subject.attach(this);
	}
 
	@Override
	public void update() {
		if(subject.getState() == "Трябват ми кукли") {
		System.out.println("Джудже : Куклата е произведена.");	
		}
		if(subject.getState() == "Трябват ми колелета") {
			System.out.println("Джудже : Колелото е произведено.");	
			}
	}    	
}

package workshop;

import java.util.ArrayList;
import java.util.List;

//public class Toys {
public class Toys {
	   private List<Workshop> observers = new ArrayList<Workshop>();
	   private String state;
	 
	   public String getState() {
	      return state;
	   }
	 
	   public void setState(String state) {
	      this.state = state;
	      notifyAllObservers();
	   }
	 
	   public void attach(Workshop observer){
	      observers.add(observer);		
	   }
	 
	   public void notifyAllObservers(){
	      for (Workshop observer : observers) {
	         observer.update();
	      }
	   } 		
	}
//}
